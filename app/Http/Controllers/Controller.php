<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    
    // Base Information

    /**
     * @OA\Info(
     *   title="Lumen Open API",
     *   version="1.0",
     *   description="",
     *   @OA\Contact(
     *     email="ronnypermadi1@gmail.com",
     *     name="Developer"
     *   )
     * )
     */

     /**
     *  @OA\Server(
     *      url=URL_CONST_HOST,
     *      description="OpenApi Host Server"
     *  )
     */

    /**
     * @OA\SecurityScheme(
     *     type="oauth2",
     *     description="Use a global client_id / client_secret and your username / password combo to obtain a token",
     *     name="Password Based",
     *     in="header",
     *     scheme="https",
     *     securityScheme="Password Based",
     *     @OA\Flow(
     *         flow="password",
     *         authorizationUrl="/oauth/authorize",
     *         tokenUrl="/oauth/token",
     *         refreshUrl="/oauth/token/refresh",
     *         scopes={}
     *     )
     * )
     */

    //  Data
    
}
